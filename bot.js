var Discord = require('discord.io');
//var logger = require('winston');
//var Sleep = require('sleep');
var auth = require('./auth.json');
var Math = require('mathjs');

var responses = require('./responses.json');
var decide_game = ["Now", "No", "Soon", "Later", "Tomorrow", "Find another teammate first", "Not right now", "Different game"];

//local variables
var lastUserToChat;
var randNum;


// Configure logger settings
/*logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';*/
// Initialize Discord Bot
var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});
bot.on('ready', function (evt) {
    //logger.info('Connected');
    //logger.info('Logged in as: ');
    //logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('connect', function (channelID) {
    bot.sendMessage({
        to: channelID,
        message: "What's up you cool babies?"
    });
});

bot.on('message', function (user, userID, channelID, message, evt) {

    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`

    message = message.toLowerCase();

    if (message.substring(0, 5) == '!zom ') {

        var cmd = message.substring(5);//.split(' ');
        //var cmd = args;//[0];
        //args = args.splice(5);

        var maybePunctuation = message.substring(message.length - 1);
        if (maybePunctuation == "?" || maybePunctuation == "!" || maybePunctuation == "." || maybePunctuation == "~")
            cmd = cmd.substring(0, cmd.length() - 1);

        //exactly
        switch (String(cmd)) {
            case 'help':
                bot.sendMessage({
                    to: channelID,
                    message: "``` List of commands:\n" +
                    "1. ping - pong\n" +
                    "2. decide - a decision maker\n" +
                    "3. help - you are here\n" +
                    "```"
                });
                return;
            case 'who are you':
                bot.sendMessage({
                    to: channelID,
                    message: 'I am a chatbot built on NodeJS for discord. Any witty comments from me were inspired by Tiiimezombie. :relaxed:'
                });
                return;
            case "whos tiiimezombie":
            case "who's tiiimezombie":
            case 'who is tiiimezombie':
                bot.sendMessage({
                    to: channelID,
                    message: 'Best boi~ (He coded my responses and likely added me to this server!~) :relaxed:'
                });
                return;
            case 'how did you get this number':
                bot.sendMessage({
                    to: channelID,
                    message: ":shrug: Uhh how did I, Kronk? \n :confounded: Well, ya got me. By all accounts, it doesn't make sense"
                });
                return;
            case "what's up":
            case 'whats up':
            case 'what up':
            case 'sup':
                randNum = Math.round(Math.random(0, responses.sup.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.sup[randNum]
                });
            case 'hello':
            case 'hi':
                randNum = Math.round(Math.random(0, responses.greet.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.greet[randNum]
                });
            case 'good morning':
            case 'morning':
                //randNum = Math.round(Math.random(0, responses.sup.length));
                bot.sendMessage({
                    to: channelID,
                    message: "Good morning!"
                });
            case 'how are you':
                randNum = Math.round(Math.random(0, responses.howU.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.howU[randNum]
                });
                return;
            case 'thank you':
            case 'thanks':
                randNum = Math.round(Math.random(0, responses.urWelcome.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.urWelcome[randNum]
                });
                return;
            case 'i love you':
            case 'i love u':
                lastUserToChat = user;
                bot.sendMessage({
                    to: channelID,
                    message: 'I love you, too, ' + user + ". :kissing_heart:"
                });
                return;
            case 'never mind':
            case 'nevermind':
            case 'nm':
                bot.sendMessage({
                    to: channelID,
                    message: '??? \n https://vignette.wikia.nocookie.net/danganronpa/images/8/89/Danganronpa_V3_Bonus_Mode_Sonia_Nevermind_Sprite_%2812%29.png/revision/latest/scale-to-width-down/444?cb=20171115074750'
                });
                return;
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
                return;
            case 'jeer':
            case 'jeers':
                randNum = Math.round(Math.random(0, responses.jeers.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.jeers[randNum]
                });
                return;
            case 'cheer':
            case 'cheers':
                randNum = Math.round(Math.random(0, responses.cheers.length));
                bot.sendMessage({
                    to: channelID,
                    message: responses.cheers[randNum]
                });
                return;
            case 'beep':
                bot.sendMessage({
                    to: channelID,
                    message: 'Boop, son. Beep boop.'
                });
                return;
            case 'do mole people exist':
                bot.sendMessage({
                    to: channelID,
                    message: 'I am an elevator. I can go up... or down.'
                });
                return;
            case 'what is a man':
                bot.sendMessage({
                    to: channelID,
                    message: 'A miserable pile of secrets.\nBut enough talk.\nHave at you!'
                });
                return;
            case 'leave':
                bot.sendMessage({
                    to: channelID,
                    message: 'Bye everyone! :kissing_heart:'
                });
                return;
        }

        var args = cmd.split(' ');

        switch (args[0]) {
            case 'decide':
                var question = cmd.substring(7);
                //
                if (question.length > 2)
                    bot.sendMessage({
                        to: channelID,
                        message: "Re: " + question
                    });

                randNum = Math.round(Math.random(0, decide_game.length));
                bot.sendMessage({
                    to: channelID,
                    message: decide_game[randNum]
                });
                return;
            case 'quote':
            case 'quotes':
                var quoteType = cmd.substring(args[0].length + 1);
                if (quoteType == "code ment") {
                    randNum = Math.round(Math.random(0, responses.quotes_codeMent.length));
                    var step2 = responses.quotes_codeMent[randNum];
                    bot.sendMessage({
                        to: channelID,
                        message: step2.replace(/&/g, "\n")
                    });
                } else {
                    bot.sendMessage({
                        to: channelID,
                        message: "``` Quotation sources:\n" +
                    "1. Code Ment\n" +
                    "2. None\n" +
                    "3. None\n" +
                    "```"
                    });
                }
                break;
            default:
                bot.sendMessage({
                    to: channelID,
                    message: cmd + "?"
                });
                break;
        }

    } else {
        if (message.substring(message.length - 5) == 'bofa?') {
            bot.sendMessage({
                to: channelID,
                message: "BOFA DEEZ NUTS LMAO"
            });
        } else if (message == "fullmetal alchemist") {
            bot.sendMessage({
                to: channelID,
                message: "Fullmetal Alchemist"
            });
        }
    }
});
